## Overview

This repository contains a media storage service for Avadio, and is responsible for user content hosting, processing and delivery. The applied transformations include image resizing, audio file conversion, and bitrate adjustments.

## Development

Make sure Rust is installed. Then, execute the following command in your shell to build and run the application:

#### ```cargo run```

## Release

Execute the following command in your shell to create an optimized build of the application:

#### ```cargo build --release```